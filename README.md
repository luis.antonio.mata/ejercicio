# Ejercicio

Ejercicio de prueba solicitado por Idesoft

1. Utilizar Gradle o Maven como gestor de configuración del proyecto.
 
2. Utilizar el siguiente servicio base, cuya información debe estar contenida al momento de inicializar el contexto.
(No se debe volver a llamar a este servicio por cada invocación del aplicativo)
https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion?id_region=7
 
3. Incluir parámetro dentro de la operación para filtrar locales por comuna (Cerrillos, Macul, por ejemplo)
Usar cualquiera: @PathVariable / @RequestParam / @RequestBody
 
El servicio a desarrollar debe responder la siguiente información de acuerdo al filtro entregado: Nombre de local, Dirección, Teléfono, Latitud, Longitud.
 
4. La app debe tener la capacidad de responder
	application/xml
	application.json
 
5. Incluir Logs (Uso libre de librerías que estimes conveniente)
 
6. Incluir plugins al proyecto para que la solución pueda estar embebida dentro de la siguiente imagen docker:
openjdk:11.0.6-jre (disponible en docker hub).
 
7. Incluir test unitarios
 
8. Entrega de la solución a través de repositorio público (debes crearlo y entregarnos la URL para clonarlo).
 
9. Código documentado y auto descriptivo.
 
10. Versiones
	Spring Boot         (v2.2.4.RELEASE)
	Java(TM) SE Runtime Environment 18.9 (build 11.0.6+8-LTS)
	Server: Docker Engine - Community 19.03.5
Gradle Version: 6.0.1 o Maven

## Pasos para la ejecución
1. Crear imagen ejecutar 

    `docker build --tag ejercicio:1.0 .`

2. Ejecutar como contenedor

    `docker container run -d -it --rm -p 8080:8080 --name ejercicio ejercicio:1.0 `
    