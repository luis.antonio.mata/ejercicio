package org.lam.ejercicio.client;

import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.HttpContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Cliente para obtenci&oactue; de los datos para cada local por regi&oacute;n.
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@Log4j2
@Component
public class ClientRest {

    @Value("${client.service.get-local-region}")
    private String urlGetLocalRegion;

    @Value("${client.protocol}")
    private String protocol;

    @Value("${client.port}")
    private Integer port;


    /**
     *
     * @return
     */
    private HttpClientContext getHttpClientContext() {
        HttpHost httpHost = new HttpHost(urlGetLocalRegion, port, protocol);
        BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
        BasicAuthCache basicAuthCache = new BasicAuthCache();
        HttpClientContext httpClientContext = HttpClientContext.create();
        basicCredentialsProvider.setCredentials(AuthScope.ANY,
                (Credentials)new UsernamePasswordCredentials("", ""));
        basicAuthCache.put(httpHost, (AuthScheme)new BasicScheme());
        httpClientContext.setCredentialsProvider((CredentialsProvider)basicCredentialsProvider);
        httpClientContext.setAuthCache((AuthCache)basicAuthCache);

        return httpClientContext;
    }

    public StringBuilder call(){
        StringBuilder stringBuilder = new StringBuilder();
        BufferedInputStream bufferedInputStream = null;
        HttpResponse response;
        CloseableHttpClient closeableHttpClient;

        try {
            closeableHttpClient = HttpClientBuilder.create().build();
            response = closeableHttpClient.execute((HttpUriRequest)new HttpGet(urlGetLocalRegion), (HttpContext)getHttpClientContext());
            if (response.getStatusLine().getStatusCode() != 200) {
                log.info("Ha fallado la solicitud, codigo de respuesta " + response.getStatusLine().getStatusCode());
            } else {
                bufferedInputStream = new BufferedInputStream(response.getEntity().getContent());
                stringBuilder = new StringBuilder();
                int value;
                while ((value = bufferedInputStream.read()) != -1){
                    stringBuilder.append((char)value);
                }
            }
        } catch (IOException e) {
            log.warn("Se ha producido un error durante la invocación servicio proveedor.", e);
        } finally {
            try {
                if (null != bufferedInputStream){
                    bufferedInputStream.close();
                }
            } catch (IOException e) {
                log.warn("Se ha producido un error durante el cierre lectura de la respuesta.", e);
            }
        }
        return stringBuilder;
    }

}
