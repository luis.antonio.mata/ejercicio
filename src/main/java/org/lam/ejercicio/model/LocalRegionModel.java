package org.lam.ejercicio.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.lam.ejercicio.client.ClientRest;
import org.lam.ejercicio.converter.LocalRegionConverter;
import org.lam.ejercicio.to.LocalRegion;
import org.lam.ejercicio.to.LocalTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Clase que contiene toda la logica para realizar la carga de la informaci&oacute;n de locales por regi&oacute;n y
 * l&oacute;gica para el filtrado de locales por regi&oacute;n.
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@Log4j2
@Component
public class LocalRegionModel {
    /**
     * Realiza la conversi&oacute;n de los datos obtenidos de la fuente a los datos para ser entregados al usuario.
     */
    @Autowired
    private LocalRegionConverter localRegionConverter;
    /**
     * Cliente HTTP para establecer la comunicaci&oacuten con el proveedor de los datos.
     */
    @Autowired
    private ClientRest clientRest;
    /**
     * Lista que contiene objetods de tipo {@link LocalRegion}
     */
    private List<LocalRegion> allLocalRegions = null;

    /**
     * Inicializa las propiedades de la clase y carga los datos obteniendolos del proveedor
     */
    @PostConstruct
    void init(){
        StringBuilder stringBuilder;
        LocalRegion[] localRegions;
        ObjectMapper objectMapper = new ObjectMapper();

        log.info("Inicializa las propiedades");
        stringBuilder = clientRest.call();

        try {
            localRegions = objectMapper.readValue(stringBuilder.toString(), LocalRegion[].class);
            allLocalRegions = Arrays.asList(localRegions);
            log.info("Propiedades inicializadas CONTENIDO {}", stringBuilder.toString());
        } catch (JsonProcessingException e) {
            log.warn("Se ha producido un error durante la lectura de los datos publicados por el servicio de locales");
        }
    }

    /**
     * Realiza la busqueda de uno o mas locales para una determinada regi&oacute;n en el par&aacute;metro {@code regionName}.
     *
     * @return Contiene una lista con todos los locales para la region solicitada
     */
    public List<LocalTO> findLocalbyRegion(String regionName){
        List<LocalRegion> localRegions;
        List<LocalTO> localTOs = null;
        //Realiza el filtrado de los datos en base al nombre de la region
        Predicate<LocalRegion> localRegionPredicate = localRegion -> localRegion.getComunaNombre().equalsIgnoreCase(regionName);

        log.info("Inicia busqueda de locales");
        if(null != allLocalRegions){
            localRegions = allLocalRegions.stream().filter(localRegionPredicate).collect(Collectors.<LocalRegion>toList());
            if(!localRegions.isEmpty()){
                localTOs = localRegionConverter.createFromPojo(localRegions);
            }
        }

        if(null == localTOs){
            localTOs = new ArrayList<>();
        }
        log.info("Termina busqueda de locales");

        return localTOs;
    }
}
