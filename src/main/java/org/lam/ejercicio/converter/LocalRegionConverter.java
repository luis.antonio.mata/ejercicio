package org.lam.ejercicio.converter;

import org.lam.ejercicio.to.LocalRegion;
import org.lam.ejercicio.to.LocalTO;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@Component
public class LocalRegionConverter  extends Converter<LocalRegion, LocalTO> {

    private static LocalTO getLocalTO(LocalRegion localRegion) {
        return new LocalTO(localRegion.getLocalNombre(), localRegion.getLocalDireccion(),
                localRegion.getLocalTelefono(), localRegion.getLocalLat(), localRegion.getLocalLng());
    }

    @Override
    @PostConstruct
    protected void initFunction() {
        setFunction(LocalRegionConverter::getLocalTO, entity -> new LocalRegion());
    }

}