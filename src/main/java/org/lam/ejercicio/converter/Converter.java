package org.lam.ejercicio.converter;

import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *  Clase abstracta para aplicación del patron Converter
 *
 *  @param <P>
 * @param <R>
 *
 *  @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 *  @since 24/7/2020
 */
@Component
public abstract class Converter<P, R> {

    protected Function<P, R> fromPojo;
    protected Function<R, P> fromDO;

    protected abstract void initFunction();

    protected void setFunction(final Function<P, R> fromPojo, final Function<R, P> fromDO) {
        this.fromPojo = fromPojo;
        this.fromDO = fromDO;
    }

    public final P convertFromDataObject(final R dataObject) {
        return fromDO.apply(dataObject);
    }

    public final R convertFromPojo(final P pojo) {
        return fromPojo.apply(pojo);
    }

    public final List<P> createFromDataObject(final Collection<R> dataObjects) {
        return dataObjects.stream().map(this::convertFromDataObject).collect(Collectors.toList());
    }

    public final List<R> createFromPojo(final Collection<P> pojos) {
        return pojos.stream().map(this::convertFromPojo).collect(Collectors.toList());
    }

}
