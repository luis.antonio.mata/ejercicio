package org.lam.ejercicio.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.log4j.Log4j2;
import org.lam.ejercicio.model.LocalRegionModel;
import org.lam.ejercicio.to.LocalRegionResponse;
import org.lam.ejercicio.to.LocalTO;
import org.lam.ejercicio.to.ResponseTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;
import static org.springframework.util.MimeTypeUtils.APPLICATION_XML_VALUE;

/**
 * Gestiona la busqueda de locales para cada regi&oacute;n
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@Log4j2
@RestController
@RequestMapping(path = "/ejercicio")
public class LocalRegionController {
    @Autowired
    private LocalRegionModel localRegionModel;

    @ApiOperation(value = "",
            notes = "Realiza la busqueda de un local dentro de una comuna, regresa la latitud y la latitud.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = LocalRegionResponse.class)
            ,@ApiResponse(code = 204, message = "No se consiguieron locales para la region informada", response = String.class)
    })
    @GetMapping(produces = {APPLICATION_JSON_VALUE, APPLICATION_XML_VALUE}, path = "/findLocalByRegion/{regionName}")
    public LocalRegionResponse findLocalByRegion(@PathVariable(name = "regionName", required = true) String regionName){
        LocalRegionResponse localRegionResponse = null;
        List<LocalTO> localRegionList;

        localRegionResponse = new LocalRegionResponse();
        if(null != regionName || !regionName.isEmpty()){
            localRegionList = localRegionModel.findLocalbyRegion(regionName);
            localRegionResponse.setLocalRegionList(localRegionList);
            localRegionResponse.setResponseTO(new ResponseTO(LocalDateTime.now().toString(), 200, "OK"));
            if(localRegionList.isEmpty()){
                localRegionResponse.setResponseTO(new ResponseTO(LocalDateTime.now().toString(), 204,
                        "No se consiguieron locales para la region informada"));
            }else{
                log.info("Termina la busqueda de locales, se encontraron {} locales", localRegionList.size());
            }
        }else {
            localRegionResponse.setResponseTO(new ResponseTO(LocalDateTime.now().toString(), 204,
                    "No se consiguieron locales para la region informada"));
            log.warn("Campo regionName nulo o vacio");
        }

        return localRegionResponse;
    }
}
