package org.lam.ejercicio.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalesRegionResponse {
    private List<LocalRegion> localRegions;
}
