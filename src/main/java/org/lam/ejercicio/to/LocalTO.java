package org.lam.ejercicio.to;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Plantilla de datos para la representaci&oacute;n de un local
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@XmlRootElement(name = "localTO")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "address",
        "phoneNumber",
        "latitude",
        "longitude"
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalTO {
    @XmlElement
    @JsonProperty("name")
    private String name;
    @XmlElement
    @JsonProperty("address")
    private String address;
    @XmlElement
    @JsonProperty("phoneNumber")
    private String phoneNumber;
    @XmlElement
    @JsonProperty("latitude")
    private String latitude;
    @XmlElement
    @JsonProperty("longitude")
    private String longitude;
}
