package org.lam.ejercicio.to;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Define la cabecera de la respuesta
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 25/7/2020
 */
@XmlRootElement(name = "Respponse")
@JsonPropertyOrder({
        "dateTime",
        "code",
        "message"
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseTO {
    @XmlElement
    @JsonProperty("dateTime")
    private String dateTime;
    @XmlElement
    @JsonProperty("code")
    private Integer code;
    @XmlElement
    @JsonProperty("message")
    private String message;

}
