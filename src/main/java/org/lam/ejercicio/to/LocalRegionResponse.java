package org.lam.ejercicio.to;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Contiene una lista de los locales para una determinada regi&actue;n.
 *
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@XmlRootElement(name = "LocalRegionResponse")
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "response",
        "localRegionList"
})
@AllArgsConstructor
@NoArgsConstructor
@Data
public class LocalRegionResponse {
    /** Colecci&oacute;n de tipo {@link org.lam.ejercicio.to.LocalTO} */
    @XmlElement
    @JsonProperty("response")
    private ResponseTO responseTO;
    @XmlElement
    @JsonProperty("localRegionList")
    private List<LocalTO> localRegionList;
}
