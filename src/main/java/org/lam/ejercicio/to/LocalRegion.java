package org.lam.ejercicio.to;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:luis.antonio.mata@gmail.com">Luis Antonio Mata</a>
 * @since 24/7/2020
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "fecha",
        "local_id",
        "local_nombre",
        "comuna_nombre",
        "localidad_nombre",
        "local_direccion",
        "funcionamiento_hora_apertura",
        "funcionamiento_hora_cierre",
        "local_telefono",
        "local_lat",
        "local_lng",
        "funcionamiento_dia",
        "fk_region",
        "fk_comuna"
})
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocalRegion {

    @JsonProperty("fecha")
    private String fecha;
    @JsonProperty("local_id")
    private String localId;
    @JsonProperty("local_nombre")
    private String localNombre;
    @JsonProperty("comuna_nombre")
    private String comunaNombre;
    @JsonProperty("localidad_nombre")
    private String localidadNombre;
    @JsonProperty("local_direccion")
    private String localDireccion;
    @JsonProperty("funcionamiento_hora_apertura")
    private String funcionamientoHoraApertura;
    @JsonProperty("funcionamiento_hora_cierre")
    private String funcionamientoHoraCierre;
    @JsonProperty("local_telefono")
    private String localTelefono;
    @JsonProperty("local_lat")
    private String localLat;
    @JsonProperty("local_lng")
    private String localLng;
    @JsonProperty("funcionamiento_dia")
    private String funcionamientoDia;
    @JsonProperty("fk_region")
    private String fkRegion;
    @JsonProperty("fk_comuna")
    private String fkComuna;
}
