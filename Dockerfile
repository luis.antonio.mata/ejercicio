FROM openjdk:11.0.6-jre
ARG JAR_FILE=target/ejercicio.jar

EXPOSE 8080
WORKDIR /usr/local/runme
COPY ${JAR_FILE} "ejercicio.jar"

ENTRYPOINT ["java", "-jar", "ejercicio.jar"]